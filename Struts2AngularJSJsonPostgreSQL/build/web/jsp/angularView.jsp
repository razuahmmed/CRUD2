<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Angular JS</title>
        <script src="<%= request.getContextPath()%>/anlularjs/angular.min.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/anlularjs/script.js" type="text/javascript"></script>
        <link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body ng-app="myModule">
        <div ng-controller="myController">
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Role</th>
                        <th>Date Of Birth</th>
                        <th>Like</th>
                        <th>Dislike</th>
                        <th>Like/Dislike</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="employee in employees">
                        <td>{{employee.number}}</td>
                        <td>{{employee.firstName}}</td>
                        <td>{{employee.lastName}}</td>
                        <td>{{employee.role}}</td>
                        <td>{{employee.dateOfBirth}}</td>
                        <td>{{employee.likes}}</td>
                        <td>{{employee.disLikes}}</td>
                        <td>
                            <button type="button" ng-click="incrementLikes(employee)">Like</button>
                            <button type="button" ng-click="incrementDislikes(employee)">Dislike</button>
                        </td>
                        <td><img ng-src="{{employee.image}}" width="200" height="150"</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>

    <script type="text/javascript">
        var app = angular.module("myModule", [])
                .controller("myController", function ($scope) {
                    var employees = [
                        {number: "1", firstName: "Saeed", lastName: "Anwar", role: "Opening Batsman", dateOfBirth: "06/09/1968", likes: 0, disLikes: 0, image: "images/anwar.jpg"},
                        {number: "2", firstName: "Shahid", lastName: "Afridi", role: "Allrounder", dateOfBirth: "01/03/1980", likes: 0, disLikes: 0, image: "images/afridi.jpg"},
                        {number: "3", firstName: "Ijaz", lastName: "Ahmed", role: "Top-order Batsman", dateOfBirth: "20/09/1968", likes: 0, disLikes: 0, image: "images/ijaz.jpg"},
                        {number: "4", firstName: "Inzamam", lastName: "-ul-Haq", role: "Middle-order Batsman", dateOfBirth: "03/03/1970", likes: 0, disLikes: 0, image: "images/inzamam.jpg"},
                        {number: "5", firstName: "Saleem", lastName: "Malik", role: "Middle-order Batsman", dateOfBirth: "16/04/1963", likes: 0, disLikes: 0, image: "images/malik.jpg"},
                        {number: "6", firstName: "Moin", lastName: "Khan", role: "Wicketkeeper Batsman", dateOfBirth: "23/09/1971", likes: 0, disLikes: 0, image: "images/moin.jpg"},
                        {number: "7", firstName: "Abdul", lastName: "Razzaq", role: "Allrounder", dateOfBirth: "02/12/1979", likes: 0, disLikes: 0, image: "images/razzaq.jpg"},
                        {number: "8", firstName: "Azhar", lastName: "Mahmood", role: "Allrounder", dateOfBirth: "28/02/1975", likes: 0, disLikes: 0, image: "images/mahmood.jpg"},
                        {number: "9", firstName: "Wasim", lastName: "Akram", role: "Allrounder", dateOfBirth: "03/06/1966", likes: 0, disLikes: 0, image: "images/wasim.jpg"},
                        {number: "10", firstName: "Saqlain", lastName: "Mushtaq", role: "Off Spiner", dateOfBirth: "29/12/1976", likes: 0, disLikes: 0, image: "images/saqlain.jpg"},
                        {number: "11", firstName: "Shoaib", lastName: "Akhtar", role: "Fast Bowler", dateOfBirth: "13/08/1975", likes: 0, disLikes: 0, image: "images/shoaib.jpg"},
                        {number: "12", firstName: "Wajahatullah", lastName: "Wasti", role: "Opening Batsman", dateOfBirth: "11/11/1974", likes: 0, disLikes: 0, image: "images/wasti.jpg"},
                        {number: "13", firstName: "Waqar", lastName: "Younis", role: "Fast Bowler", dateOfBirth: "16/11/1971", likes: 0, disLikes: 0, image: "images/waqar.jpg"},
                        {number: "14", firstName: "Mohammad", lastName: "Yousuf", role: "Middle-order Batsman", dateOfBirth: "27/08/1974", likes: 0, disLikes: 0, image: "images/yousuf.jpg"},
                        {number: "15", firstName: "Mushtaq", lastName: "Ahmed", role: "Leg Spiner", dateOfBirth: "28/06/1970", likes: 0, disLikes: 0, image: "images/mushtaq.jpg"}
                    ];

                    $scope.employees = employees;

                    $scope.incrementLikes = function (arg) {
                        arg.likes++;
                    };

                    $scope.incrementDislikes = function (arg) {
                        arg.disLikes++;
                    };
                });
    </script>
</html>
