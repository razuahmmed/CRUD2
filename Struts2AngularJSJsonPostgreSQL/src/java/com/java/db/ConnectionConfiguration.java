package com.java.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionConfiguration {

    public static final String URL = "jdbc:postgresql://localhost:3310/testgree";
    public static final String USERNAME = "postgres";
    public static final String PASSWORD = "123456";

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void main(String[] args) {
        System.out.println("Conn " + getConnection());
    }
}
